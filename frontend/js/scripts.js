"use strict";

$(function() {

	(function taskPreviewController() {
		let $form = $('[rel="js-form-addTask"]'),
			$task_preview_button = $form.find('button[rel="js-form-preview"]');

		$task_preview_button.click(handleTaskPreview);

		function handleTaskPreview(event) {
			event.preventDefault();

			let $task_list = $('[rel="js-task-list"]'),
				$preview = $("<div class='task task_preview card'><div class='card-body'></div></div>"),
				username = $form.find('input[name="username"]').val(),
				email = $form.find('input[name="email"]').val(),
				task = $form.find('textarea[name="task"]').val(),
				image = $form.find('input[name="image"]')[0].files[0],
				$username = $(`<span class="task-userInfo task-userInfo_name">Имя пользователя: ${username}</span>`),
				$email = $(`<span class="task-userInfo task-userInfo_email">Email: ${email}</span>`),
				$task = $(`<span class="task-userInfo task-userInfo_task">Задача: ${task}</span>`),
				$image;

			if (image) {
				let reader = new FileReader();
				$image = $('<img class="task-userInfo task-userInfo_image">');

				reader.onload = function(event) {
					$image.attr('src', event.target.result);
				}

				reader.readAsDataURL(image);
			}

			$preview.find('.card-body').append([$image, $username, $email, $task]);
			$task_list.find('.task_preview').remove(); // clear prior to prepending
			$task_list.prepend($preview);
		}
	})();

});
