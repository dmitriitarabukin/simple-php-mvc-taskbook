<?php

class Admin extends Controller
{
	public function index()
	{
		$this->view('admin/index');
	}

	public function login()
	{
		$username = htmlspecialchars($_POST['username']);
		$password = htmlspecialchars($_POST['password']);

		$user = $this->model('User')->findUser($username, $password);

		if ($user) {
			$_SESSION['name'] = $user->user_name;
			header('Location: ' . SITE_URL . 'home');
		} else {
			header('Location: ' . SITE_URL . 'admin');
		}
		
		die();
	}

	public function logout()
	{
		session_destroy();
		header('Location: ' . SITE_URL . 'home');
		die();
	}
}