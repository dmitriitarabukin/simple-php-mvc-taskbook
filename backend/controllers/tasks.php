<?php

use Aws\S3\S3Client;

class Tasks extends Controller
{
	private $upload_image_width_constraint = 320;
	private $upload_image_height_constraint = 240;

	public function index()
	{
		$this->view('task/index');
	}

	public function add()
	{
		if(!isset($_POST)) die();

		$task = $this->model('Task');

		if (!empty($_FILES['image']['name'])) {
			$temp_image = $_FILES['image']['tmp_name'];
			$image_name = $_FILES['image']['name'];
			$target_directory = DOCUMENT_ROOT .'/backend/temp/';
			$target_image = $target_directory . basename($image_name);
			$image_file_type = strtolower(pathinfo($target_image, PATHINFO_EXTENSION));

			// TODO: create a separate class for aws s3 storage
			$aws_s3_client = S3Client::factory(
				[
					'credentials' => [
						'key' => $task->aws_access_key_id,
						'secret' => $task->aws_secret_access_key
					],
					'version' => 'latest',
					'region'  => 'us-east-2'
				]
			);
	
			if (!$this->isImageTypeValid($image_file_type)) {
				echo 'Неверный тип файла. Разрешенные форматы: jpg, png, gif';
				die();
			}
	
			if ($this->doesFileExist($target_image)) {
				echo 'Не удалось загрузить файл. Файл с таким именем уже существует.';
				die();
			}
	
			if (!$this->isImageSizeCorrect($temp_image)) {
				if (!$this->resizeImage($temp_image, $target_image)) {
					echo 'Не удалось загрузить файл. Ошибка при конвертации файла.';
					die();
				}
			} else {
				if (!move_uploaded_file($temp_image, $target_image)) {
					echo 'Не удалось загрузить файл.';
					die();
				}
			}

			// TODO: handle aws s3 exceptions
			// Put image to aws s3 storage
			$aws_s3_client->putObject(
				[
					'Bucket' => $this->s3_bucket,
					'Key' => basename($image_name),
					'SourceFile' => $target_image,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				]
			);
		}

		$task_data = [
			'username' => $_POST['username'],
			'email' => $_POST['email'],
			'task' => $_POST['task'],
			'image_path' => isset($image_name) ? $image_name : '',
			'status' => 0
		];

		$result = $task->add($task_data);

		header('Location: ' . SITE_URL . 'home');
	}

	public function edit($params = [])
	{
		if (!isset($_SESSION['name']) && $_SESSION['name'] != 'admin') {
			header('Location: ' . SITE_URL . 'home');
		}

		$task_id = $params[0];

		$task = $this->model('Task');
		$task->get($task_id);

		$this->view('task/edit', ['task' => $task->task]);
	}

	public function update()
	{
		if (!isset($_SESSION['name']) && $_SESSION['name'] != 'admin') {
			header('Location: ' . SITE_URL . 'home');
		}
		
		$id = htmlspecialchars($_POST['id']);
		$new_task = isset($_POST['task']) ? htmlspecialchars($_POST['task']) : '';
		$new_status = htmlspecialchars($_POST['status']) == 'on' ? 1 : 0;
		$task = $this->model('Task');
		$task->update($id, $new_task, $new_status);

		header('Location: ' . SITE_URL . 'home');
	}

	private function isImageTypeValid($image_file_type)
	{
		if ($image_file_type == 'jpg'	|| $image_file_type == 'png' || $image_file_type == 'gif') {
			return true;
		}
		return false;
	}

	private function doesFileExist($file) {
		if (file_exists($file)) return true;
		return false;
	}

	private function isImageSizeCorrect($image) {
		$image_size_array = $this->getImageSize($image);
		if ($image_size_array[0] > $this->upload_image_width_constraint || $image_size_array[1] > $this->upload_image_height_constraint) return false;
		return true;
	}

	private function resizeImage($source_image, $target_image) {
		$image_size_array = $this->getImageSize($source_image);
		$width_ratio = $this->upload_image_width_constraint / $image_size_array[0];
		$height_ratio = $this->upload_image_height_constraint / $image_size_array[1];

		$chosen_ratio = min($width_ratio, $height_ratio);
		$new_width = $image_size_array[0] * $chosen_ratio;
		$new_height = $image_size_array[1] * $chosen_ratio;
		$new_image = imagecreatetruecolor($new_width, $new_height);

		switch(strtolower(image_type_to_mime_type($image_size_array[2]))) {
			case 'image/jpeg':
				$image = imagecreatefromjpeg($source_image);
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $image_size_array[0], $image_size_array[1]);

				if (imagejpeg($new_image, $target_image)) {
					$this->freeMemoryAfterImageResizing($image, $new_image);
					return true;
				}
				break;
			case 'image/png':
				$image = imagecreatefrompng($source_image);
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $image_size_array[0], $image_size_array[1]);

				if (imagepng($new_image, $target_image)) {
					$this->freeMemoryAfterImageResizing($image, $new_image);
					return true;
				}
				break;
			case 'image/gif':
				// doesn't preserve animation while resizing
				$image = imagecreatefromgif($source_image);
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $image_size_array[0], $image_size_array[1]);

				if (imagegif($new_image, $target_image)) {
					$this->freeMemoryAfterImageResizing($image, $new_image);
					return true;
				}
				break;
			default:
				return false;
		}
	}

	private function getImageSize($image) {
		list($image_width, $image_height, $type) = getimagesize($image);
		return [$image_width, $image_height, $type];
	}

	private function freeMemoryAfterImageResizing($image1, $image2) {
		imagedestroy($image1);
		imagedestroy($image2);
	}
}