<?php

use Aws\S3\S3Client;

class Home extends Controller
{
	public function index($params = [])
	{
		$current_page = isset($params[0]) && is_numeric($params[0]) ? $params[0] : 1;
		$tasks_per_page = 3;

		$available_orders = ['username', 'email', 'status'];
		$available_directions = ['asc', 'desc'];
		$chosen_order_by = isset($params[1]) ? $params[1] : isset($_POST['order']) ? $_POST['order'] : 'username';
		$chosen_direction = isset($params[2]) ? $params[2] : isset($_POST['direction']) ? $_POST['direction'] : 'asc';
		$order_key = array_search($chosen_order_by, $available_orders);
		$direction_key = array_search($chosen_direction, $available_directions);
		$order_by = $available_orders[$order_key];
		$direction = $available_directions[$direction_key];

		$task = $this->model('Task');
		$task->getAll($order_by, $direction, $tasks_per_page, ($current_page - 1) * $tasks_per_page);

		// TODO: create a separate class for aws s3 storage
		$aws_s3_client = S3Client::factory(
			[
				'credentials' => [
					'key' => $this->aws_access_key_id,
					'secret' => $this->aws_secret_access_key
				],
				'version' => 'latest',
				'region'  => 'us-east-2'
			]
		);

		// TODO: handle aws s3 exceptions
		// Get images' urls from aws s3 storage
		foreach($task->tasks as $key => $current_task) {
			$cmd = $aws_s3_client->getCommand('GetObject', [
    		'Bucket' => $this->s3_bucket,
    		'Key'    => $current_task->image_path
			]);
			$request = $aws_s3_client->createPresignedRequest($cmd, '+10 minutes');
			$task->tasks[$key]->image_path = (string) $request->getUri();
		}

		$task->getNumberOfTasks();
		
		$this->view('home/index', [
			'tasks' => $task->tasks, 
			'order' => $order_by, 
			'direction' => $direction, 
			'current_page' => $current_page,
			'tasks_per_page' => $tasks_per_page,
			'number_of_tasks' => $task->number_of_tasks,
			'params' => $params
		]);
	}
}