<?php

class Task extends Database
{
	public $tasks;
	public $task;
	public $number_of_tasks;

	public function getNumberOfTasks()
	{
		$this->number_of_tasks = self::query('SELECT COUNT(*) as count FROM tasks')[0]->count;
	}

	public function getAll($order_by, $direction, $tasks_per_page, $page_offset)
	{
		// TODO: PDO casts prepared params to string. Bind limit and offset params explicitly casting int type
		$this->tasks = self::query("SELECT * FROM (SELECT * FROM tasks LIMIT $tasks_per_page OFFSET $page_offset) tasks_subquery ORDER BY $order_by $direction");
	}

	public function get($id)
	{
		$this->task = self::query('SELECT * FROM tasks WHERE id = ?', [$id])[0];
	}

	public function add($task_data)
	{
		return self::query('INSERT INTO tasks (username, email, task, image_path, status) VALUES (?, ?, ?, ?, ?);', array_values($task_data));
	}

	public function update($id, $task, $status)
	{
		self::query("UPDATE tasks SET task = ?, status = ? WHERE id = ?", [$task, $status, $id]);
	}
}