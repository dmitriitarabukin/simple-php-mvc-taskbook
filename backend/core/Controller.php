<?php

class Controller
{
	protected $aws_access_key_id;
	protected $aws_secret_access_key;
	protected $s3_bucket;

	public function __construct()
	{
		$this->aws_access_key_id = getenv('AWS_ACCESS_KEY_ID');
		$this->aws_secret_access_key = getenv('AWS_SECRET_ACCESS_KEY');
		$this->s3_bucket = getenv('S3_BUCKET');
	}
	// TODO: possibility to pass parameters to model constructor?
	protected function model($model)
	{
		require_once '../backend/models/' . $model . '.php';
		return new $model();
	}

	protected function view($view, $data = [])
	{
		require_once '../backend/templates/header.php';
		require_once '../backend/views/' . $view . '.php';
		require_once '../backend/templates/footer.php';
	}
}