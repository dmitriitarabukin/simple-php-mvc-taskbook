<?php

class Component 
{
	private $views_directory = 'views';
	private $view = 'default.php';
	private $path_to_component = '';

	protected function __construct($view)
	{
		$this->path_to_component = 	DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . get_class($this);

		if (isset($view))
			$this->view = $view . '.php';

		$this->display();
	}

	private function display()
	{
		include $this->path_to_component . DIRECTORY_SEPARATOR . $this->views_directory . DIRECTORY_SEPARATOR . $this->view;
	}
}