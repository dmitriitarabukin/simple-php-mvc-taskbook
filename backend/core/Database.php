<?php

class Database
{
	protected $host;
	protected $dbName;
	protected $username;
	protected $password;

	public function __construct()
	{
		$this->host = getenv('DB_HOST');
		$this->dbName = getenv('DB_NAME');
		$this->username = getenv('DB_USER_NAME');
		$this->password = getenv('DB_PASSWORD');
	}

	private function connect() {
		$pdo = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbName, $this->username, $this->password);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
	}

	protected function query($query, $params = [])
	{
		$statement = $this->connect()->prepare($query);
		$statement->execute($params);

		if (explode(' ', $query)[0] == 'SELECT' && $statement) {
			$data = $statement->fetchAll(PDO::FETCH_CLASS);
			return $data;
		}

		if (explode(' ', $query)[0] == 'INSERT' && $statement) {
			return true;
		}

		return false;
	}
}