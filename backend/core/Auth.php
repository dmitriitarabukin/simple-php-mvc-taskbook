<?php 

class Auth
{
	public static function startSession()
	{
		session_start();
	}

	public static function isUserAdmin()
	{
		if (isset($_SESSION['name']) && $_SESSION['name'] == 'admin') {
			return true;
		}

		return false;
	}
}