<?php

class Autoloader
{
	public static function init()
	{
		spl_autoload_register('self::autoload');
	}

	private static function autoload($class, $dir = null)
	{
		// TODO: recursive subdirectory class autoload
		$sources = [
			DOCUMENT_ROOT . "/backend/core/$class.php",
			DOCUMENT_ROOT . "/backend/components/Pagination/$class.php",
			DOCUMENT_ROOT . "/backend/components/Sort/$class.php",
		];
		
		foreach ($sources as $source) {
			if (file_exists($source)) {
				require_once $source;
			}
		}
	}
}