<?php

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SITE_URL', (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/');
define('PUBLIC_PATH', $_SERVER['DOCUMENT_ROOT'] . '/frontend/js/');