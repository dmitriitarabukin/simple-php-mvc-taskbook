<div class="row">
	<div class="col-12 col-sm-4">
		<section>
			<form class="form_task" action="<?php echo SITE_URL ?>tasks/update" method="POST">
				<input type="hidden" name="id" value="<?php echo $data['task']->id ?>">
				<h4>Редактировать задачу</h4>
				<div class="form-group">
					<label for="task">Задача:</label>
					<textarea class="form-control" id="task" name="task" rows="6"><?php echo $data['task']->task ?></textarea>				
				</div>
				<div class="form-check">
  				<input class="form-check-input" type="checkbox" id="status" name="status" <?php echo $data['task']->status == true ? 'checked' : '' ?>>
  				<label class="form-check-label" for="status">Выполнено?</label>
				</div>		
				<button class="btn btn-primary" type="submit">Применить</button>
			</form>
		</section>
	</div>
</div>