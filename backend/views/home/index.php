<div class="row">
	<div class="col-12 col-sm-4">
		<section>
			<form class="form_task mb-5 mb-sm-0" action="/tasks/add" method="POST" enctype="multipart/form-data" rel="js-form-addTask">
				<h4>Добавить задачу</h4>
				<div class="form-group">
					<label for="username">Имя:</label>
					<input class="form-control" type="text" id="username" name="username" placeholder="Введите Ваше имя">
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input class="form-control" type="text" id="email" name="email" placeholder="Введите Ваш Email">		
				</div>
				<div class="form-group">
					<label for="task">Задача:</label>
					<textarea class="form-control" type="text" id="task" name="task"></textarea>				
				</div>
				<div class="form-group">
					<input class="form-control-file" type="file" id="image" name="image">
					<label for="image">Выбрать картинку</label>
				</div>				
				<button class="btn btn-primary mb-sm-2 mb-lg-0" type="submit">Отправить</button>
				<button class="btn btn-secondary" type="submit" rel="js-form-preview">Предпросмотр</button>
			</form>
		</section>
	</div>

	<div class="col-12 col-sm-8">
		<h3>Список задач</h3>	
		<section id="task-list">
			<?php if (count($data['tasks']) > 0): ?>
				<?php
					new Sort([
						'order' => $data['order'], 
						'direction' => $data['direction']
					]);
				?>
			<?php endif; ?>
			<div rel="js-task-list">			
				<?php if (count($data['tasks']) > 0): ?>
					<?php foreach($data['tasks'] as $task): ?>
						<div class="task card">
						  <div class="card-body">
						  	<?php if (!empty($task->image_path)): ?>
									<img class="task-userInfo task-userInfo_image" src="<?php echo $task->image_path ?>">
								<?php endif; ?>
								<span class="task-userInfo task-userInfo_name">Имя пользователя: <?php echo $task->username ?></span>
								<span class="task-userInfo task-userInfo_email">Email: <?php echo $task->email ?></span>
								<span class="task-userInfo task-userInfo_task">Задача: <?php echo $task->task ?></span>
								<span class="task-userInfo task-userStatus_status">Статус: <?php echo $task->status > 0 ? '<span class="text-success">&check;</span>' : '<span class="text-danger">&cross;</span>' ?></span>
								<?php if (Auth::isUserAdmin()): ?>
									<a href="/tasks/edit/<?php echo $task->id ?>" class="task-edit btn btn-secondary">Редактировать</a>
								<?php endif; ?>
						  </div>
						</div>
					<?php endforeach; ?>	
				<?php endif; ?>
			</div>
		</section>
	
		<?php if (floor($data['number_of_tasks'] / ($data['tasks_per_page'] + 1)) > 0): ?>
			<?php 
				new Pagination([
					'current_page' => $data['current_page'], 
					'tasks_per_page' => $data['tasks_per_page'], 
					'number_of_tasks' => $data['number_of_tasks']
				], 
				$data['params']);
			?>
		<?php endif; ?>
	
	</div>	
</div>

