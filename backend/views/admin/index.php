<div class="row justify-content-center">
	<div class="col-12 col-sm-4">
		<section>
			<form class="form_adminLogin" action="/admin/login" method="POST">
				<h4>Вход для администратора</h4>
				<div class="form-group">
					<label for="username">Имя</label>
					<input class="form-control" type="text" id="username" name="username" placeholder="Введите Ваше имя">
				</div>
				<div class="form-group">
					<label for="password">Пароль</label>
					<input class="form-control" type="text" id="password" name="password" placeholder="Введите пароль">		
				</div>			
				<button class="btn btn-primary" type="submit">Войти</button>
			</form>
		</section>
	</div>	
</div>
