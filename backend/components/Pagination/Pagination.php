<?php

class Pagination extends Component
{
	protected $current_page;
	protected $tasks_per_page;
	protected $number_of_tasks;
	protected $new_url;

	public function __construct($params, $url_params, $view = NULL) 
	{
		$this->current_page = $params['current_page'];
		$this->tasks_per_page = $params['tasks_per_page'];
		$this->number_of_tasks = $params['number_of_tasks'];

		unset($url_params[0]);
		$this->new_url = '/' . implode('/', $url_params);

		parent::__construct($view);
	}
}