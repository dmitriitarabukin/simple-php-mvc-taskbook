<nav class="d-flex justify-content-center" aria-label="Page navigation">

	<ul class="pagination">

	<?php if($this->current_page > 1): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . ($this->current_page - 1) . $this->new_url ?>" aria-label="Previous">
				<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
	<?php endif; ?>

	<?php if ($this->current_page > 2): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . '1' . $this->new_url ?>">1</a>
		</li>
	<?php endif; ?>

	<?php if ($this->current_page > 3): ?>
		<li class="page-item">...</li>
	<?php endif; ?>

	<?php if ($this->current_page - 1 > 0): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . ($this->current_page - 1) . $this->new_url ?>"><?php echo ($this->current_page - 1) ?></a>
		</li>
	<?php endif; ?>

		<li class="page-item active" aria-current="page">
			<a class="page-link" href="#"><?php echo $this->current_page ?>
				<span class="sr-only">(current)</span>
			</a>
		</li>

	<?php if ($this->current_page + 1 < ceil($this->number_of_tasks / $this->tasks_per_page) + 1): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . ($this->current_page + 1) . $this->new_url ?>"><?php echo ($this->current_page + 1) ?></a>
		</li>
	<?php endif; ?>

	<?php if ($this->current_page < ceil($this->number_of_tasks / $this->tasks_per_page) - 2): ?>
		<li class="page-item">...</li>
	<?php endif; ?>

	<?php if ($this->current_page < ceil($this->number_of_tasks / $this->tasks_per_page) - 1): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . ceil($this->number_of_tasks / $this->tasks_per_page) . $this->new_url ?>"><?php echo ceil($this->number_of_tasks / $this->tasks_per_page) ?></a>
		</li>
	<?php endif; ?>

	<?php if ($this->current_page < ceil($this->number_of_tasks / $this->tasks_per_page)): ?>
		<li class="page-item">
			<a class="page-link" href="<?php echo SITE_URL . ($this->current_page + 1) . $this->new_url ?>" aria-label="Next">
				<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	<?php endif; ?>

	</ul>

</nav>