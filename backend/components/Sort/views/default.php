<form class="sort" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
	<small>Сортировать по:</small>
	<div class="form-row">
	<div class="form-group col-12 col-sm-6">
	<select class="form-control" name="order">
		<option value="username" <?php echo $this->order == 'username' ? "selected" : "" ?>>Имя пользователя</option>
		<option value="email" <?php echo $this->order == 'email' ? "selected" : "" ?>>Email</option>
		<option value="status" <?php echo $this->order == 'status' ? "selected" : "" ?>>Статус</option>
	</select>
	</div>
	<div class="form-group col-12 col-sm-6">
	<select class="form-control" name="direction">
		<option value="asc" <?php echo $this->direction == 'asc' ? "selected" : "" ?>>По возрастанию</option>
		<option value="desc" <?php echo $this->direction == 'desc' ? "selected" : "" ?>>По убыванию</option>
	</select>
	</div>
	</div>
	<button class="btn btn-primary" type="submit">Применить</button>
</form>