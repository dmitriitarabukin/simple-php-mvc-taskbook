<?php

class Sort extends Component
{
	protected $order;
	protected $direction;

	public function __construct($params, $view = NULL)
	{
		$this->order = $params['order'];
		$this->direction = $params['direction'];

		parent::__construct($view);
	}
}