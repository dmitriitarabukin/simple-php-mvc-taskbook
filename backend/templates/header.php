<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Задачник</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="/frontend/css/styles.css">
	<link rel="icon"
      href="/frontend/favicon.ico">
</head>
<body>
	<nav class="navbar navbar-expand-sm navbar-light" style="background-color: #e3f2fd;">
		<div class="container">
			<a class="navbar-brand" href="/home">Задачник</a>
    	<ul class="navbar-nav">
        <?php if (Auth::isUserAdmin()): ?>
        	<li class="nav-item">
            <a class="nav-link float-right" href="/admin/logout">Выход</a>
          </li>
        <?php else: ?>
					<li class="nav-item">
    	  	  <a class="nav-link" href="/admin">Админ</a>
    	  	</li>
        <?php endif; ?>
        </ul>
		</div>	  
	</nav>
	<div id="content">
		<div class="container">
